<?php

/*
 * https://medium.com/philipobenito/dependency-injection-as-a-tool-for-testing-902c21c147f1
 * */

class Foo
{
    protected $bar;

    public function doSomething($bar, $id, $name, $email)
    {
        $this->bar = $bar;
        // .. possibly manipulate $id, $name and $email

        // assume we want to edit a row in a database and
        // then return an array of data containing that row
        return $this->bar->editAndReturnUser($id, $name, $email);
    }
}

/*
 * Testing with a Fake
 * */

class FakeBar
{
    public $users = [
        1 => [
            'name' => 'Adam',
            'email' => 'adam@example.com'
        ]
    ];

    public function editAndReturnUser($id, $name, $email)
    {
        $this->users[$id]['name'] = $name;
        $this->users[$id]['email'] = $email;

        return $this->users[$id];
    }
}

class FooTestFake
{
    public function testDoSomethingEditsDataAndReturnsArray()
    {
        $foo = new Foo();
        $bar = new FakeBar();

        $user = $foo->doSomething($bar, 1, 'Phil', 'hello@example.com');

        $this->assertInternalType('array', $user, 'The return of (doSomething) was not an array');
        // .. assert that Bar has changed the state of the database
    }
}

/*
 * Testing with a Stub
 * */
class FooTestStub extends PHPUnit_Framework_Testcase
{
    public function testDoSomethingEditsDataAndReturnsArray()
    {
        $foo = new Foo();

        $user = [
            'name' => 'Phil',
            'email' => 'hello@example.com'
        ];

        $stubBar = $this->getMock('Bar');

        $stubBar->expects($this->any())
            ->method('editAndReturnUser')
            ->will($this->returnValue($user));

        $foo->doSomething($stubBar, 1, 'Phil', 'hello@example.com');

        $this->assertInternalType('array', $user, 'The return of (doSomething) was not an array');
        $this->assertSame($user['name'], 'Phil', 'The returned user name was not as expected');
        $this->assertSame($user['email'], 'hello@example.com', 'The returned user email was not as expected');
    }
}

/*
 * Testing with a Mock
 * */

class FooTestMock extends PHPUnit_Framework_Testcase
{
    public function testDoSomethingEditsDataAndReturnsArray()
    {
        $foo = new Foo();

        $user = [
            'name' => 'Phil',
            'email' => 'hello@example.com'
        ];

        $mockBar = $this->getMock('Bar');

        $mockBar->expects($this->once())
            ->method('editAndReturnUser')
            ->with($this->equalTo(1), $this->equalTo('Phil'), $this->equalTo('hello@example.com'))
            ->will($this->returnValue($user));

        $foo->doSomething($mockBar, 1, 'Phil', 'hello@example.com');

        $this->assertInternalType('array', $user, 'The return of (doSomething) was not an array');
        $this->assertSame($user['name'], 'Phil', 'The returned user name was not as expected');
        $this->assertSame($user['email'], 'hello@example.com', 'The returned user email was not as expected');
    }
}
