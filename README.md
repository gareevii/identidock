# Identidock

Небольшой самописный PHP проект. 
Целью которого является изучение разнообразных технологий, инструментов и методик программирования на php. 
В качестве стартового функционала используется сторонний докер образ amouat/dnmonster.

## Installation

Для удобства разработки, все необходимое находится в одном репозитории.

```bash
 git clone git@gitlab.com:gareevii/identidock.git
```
```bash
 php app/composer.phar -dapp i
```

## Usage
В терминале запустить:
```
 docker-compose --project-directory docker up -d
```
Cервис генерации картинок - [localhost](http://localhost)

Cервис генерации данных для бд (запуск отключен) - [localhost:8000](http://localhost:8000/)

Запуск некоторых контейнеров по умолчанию отключен в docker-compose.yml:
```
 profiles:
 - donotstart
```

## Dev Tools
[Roave Security Advisories](https://github.com/Roave/SecurityAdvisories)

```
 Prevents installation of composer packages with known security vulnerabilities: no API, simply require it
```

[The PHP Unit Testing framework.](https://github.com/sebastianbergmann/phpunit)
```bash
 composer exec phpunit
```
[Codeception]( https://github.com/Codeception/Codeception.git) - BDD-style testing framework
```bash
composer exec codecept run
```
> [official](https://codeception.com)
>
[PhpCsFixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) - A tool to automatically fix PHP code style
```bash
 composer exec php-cs-fixer fix 
```
[PHPStan](https://github.com/phpstan/phpstan) - PHP Static Analysis Tool
```bash
 composer exec phpstan
```
> [official](https://phpstan.org/)
> 
[Psalm](https://github.com/vimeo/psalm) - A static analysis tool for finding errors in PHP applications
```bash
 composer exec psalm
```
## License
[MIT](https://choosealicense.com/licenses/mit/)