<?php

require __DIR__ . '/../vendor/autoload.php';

// https://github.com/longxinH/xhprof/
// https://www.kam24.ru/xhprof/docs/index.html#ini_file
/*xhprof_enable(
    XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY,
    [
        'ignored_functions' =>
            [
                'getenv',
                'Redis::__construct',
                'Redis::pconnect',
                'Redis::get',
                'xhprof_disable',
                'header'
            ]
    ]
);*/

// TODO: рассмотреть вариант использования confd
const DNMONSTER_HOST = 'dnmonster';
const DNMONSTER_PORT = '8080';

const REDIS_HOST = 'redis';
const REDIS_PORT = 6379;
const REDIS_TIMEOUT = 2.5;

const MYSQL_DB_HOST = 'mysql';
const MYSQL_DB_USER = 'user-dev';
const MYSQL_DB_USER_PASSWORD = 'pw-user-dev';
const MYSQL_DB_NAME = 'identidock_dev';

try {
    // mysql пока не нужен, но подключаемся для проверки
    // https://www.php.net/manual/ru/pdo.connections.php
    $dbh = new PDO(
        'mysql:host=' . MYSQL_DB_HOST . ';dbname=' . MYSQL_DB_NAME,
        MYSQL_DB_USER,
        MYSQL_DB_USER_PASSWORD,
        [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            // для php < 8.0, в php >= 8 режим по умолчанию
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    );

    $name = !empty($_GET['name']) ? filter_input(INPUT_GET, $_GET['name'], FILTER_SANITIZE_STRING) : 'noname';

    // https://github.com/phpredis/phpredis
    $redis = new Redis();

    if (!$redis->pconnect(REDIS_HOST, REDIS_PORT, REDIS_TIMEOUT)) {
        throw new RedisException("Can't connect to redis host '" . REDIS_HOST . "'");
    }

    $content = $redis->get($name);

    if (empty($content)) {
        // ATTENTION!!! USING urlencode()
        // $imageUrl = urlencode('http://' . LOCALHOST . ':' . DNMONSTER_PORT . "/monster/$name?size=80");
        // $content = file_get_contents($imageUrl); - EMPTY RESPONSE

        $imageUrl = 'http://' . DNMONSTER_HOST . ':' . DNMONSTER_PORT . "/monster/$name?size=80";
        $content = file_get_contents($imageUrl);

        /*if (empty($content)) {
            // в браузер ничего не попадает, Status Code: 200.
            // сервер: 127.0.0.1:60704 [500]: GET /image.php?name=DEV%20123 - Uncaught App\Exceptions\Services\IdentidockServiceException
            throw new IdentidockServiceException("Can't get image for name '$name'", 500);
        }*/

        $redis->set($name, $content);
    }

    // send image to output
    header('Content-type: image/png');
    echo $content;

    // stop profiler
    // $xhprof_data = xhprof_disable();
    // print_r($xhprof_data);
    exit;
} catch (PDOException | RedisException $e) {
    //TODO: разделить и написать разные обработчики
    // Если исключение не будет перехвачено и обработано, PHP выдаст фатальную ошибку: "Uncaught Exception ..." (Неперехваченное исключение)
    throw $e;
} catch (Throwable $t) {
    //TODO: написать обработчик
    // Если исключение не будет перехвачено, PHP выдаст фатальную ошибку: "Uncaught Exception ..." (Неперехваченное исключение)
    http_response_code($t->getCode());
    throw $t;
}
