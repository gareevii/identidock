<?php

require __DIR__ . '/../vendor/autoload.php';

try {
    // инициализация приложения
    $app = new Core\Application();
    $app->run();
} catch (Throwable $e) {
    throw $e;
}

// TODO: временно, пока скрипт запускается напрямую в storm
if (getenv('PHP_STORM')) {
    echo 'excelent';
    exit;
}

$name = $_POST['name'] ?? 'noname';
?>

<html lang="ru">
    <head>
        <title>Identidock</title>
    </head>
    <body>
        <form method="POST">Hello
            <input type="text" name="name" value="<?php echo htmlspecialchars($name, ENT_QUOTES)?>">
            <input type="submit" value="submit">
        </form>
        <p>You look like a:
            <img src='image.php?name=<?php echo htmlspecialchars($name, ENT_QUOTES)?>' alt="image"/>
        <p>
    </body>
</html>