<?php

namespace Core;

use Core\Configs\Config;
use Core\PhpIni\IniSetTestEnv;
use Core\Configs\ConfigTestEnv;
use Core\PhpIni\IniSetInterface;

class ApplicationFactoryTestEnv implements ApplicationFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createConfig(): Config
    {
        return new ConfigTestEnv();
    }

    /**
     * @inheritDoc
     */
    public function createIniSet(): IniSetInterface
    {
        return new IniSetTestEnv();
    }
}
