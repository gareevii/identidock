<?php

namespace Core;

use Core\Configs\Config;
use Core\PhpIni\IniSetInterface;

interface ApplicationFactoryInterface
{
    /**
     * @return Config
     */
    public function createConfig(): Config;

    /**
     * @return IniSetInterface
     */
    public function createIniSet(): IniSetInterface;
}
