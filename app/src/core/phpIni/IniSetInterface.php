<?php

namespace Core\PhpIni;

interface IniSetInterface
{
    /**
     * Обработка вывода ошибок
     *
     * @return bool
     */
    public function displayErrors(): bool;
}
