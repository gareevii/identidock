<?php

namespace Core\PhpIni;

final class Turn
{
    public const ON = '1';
    public const OFF = '0';
}
