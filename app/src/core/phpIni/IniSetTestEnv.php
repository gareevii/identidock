<?php

namespace Core\PhpIni;

class IniSetTestEnv implements IniSetInterface
{
    /**
     * @inheritDoc
     */
    public function displayErrors(): bool
    {
        // вывод ошибок на экран вместе с остальным выводом
        if (!ini_get('display_errors')) {
            ini_set('display_errors', Turn::ON);
        }

        // вывод ошибок возникающие во время запуска php
        if (!ini_get('display_startup_errors')) {
            ini_set('display_startup_errors', Turn::ON);
        }

        // get all PHP errors
        error_reporting(E_ALL);

        return true;
    }
}
