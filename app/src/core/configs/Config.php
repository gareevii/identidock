<?php

namespace Core\Configs;

use App\core\configs\ConfigException;

class Config
{
    public const CONFIGS_PATH = __DIR__ . '/../../configs';
    public const MAIN_CONFIG_FILE_NAME = 'main.config.yml';
    public const CONFIG_FILE_NAME = 'config.yml';

    /**
     * @var array<string, mixed>
     */
    private array $configs;

    /**
     * @var string
     */
    protected string $confDir;

    public function __construct()
    {
        $this->configs = $this->parseConfigs();
    }

    /**
     * @return array<string, mixed>
     * @throws ConfigException
     */
    private function parseConfigs(): array
    {
        $mainConfigsPath = self::CONFIGS_PATH . '/' . self::MAIN_CONFIG_FILE_NAME;
        $mainConfigs = yaml_parse_file($mainConfigsPath);

        if (empty($this->confDir)) {
            throw new ConfigException('Undefinde config path');
        }

        $configsPath = self::CONFIGS_PATH . '/' . $this->confDir . '/' . self::CONFIG_FILE_NAME;
        $configs = yaml_parse_file($configsPath);

        // TODO: рекурсивно смержить $mainConfigs с $configs с заменой

        return $configs;
    }

    /**
     * @return array<string, mixed>
     */
    public function getConfigs(): array
    {
        return $this->configs;
    }
}
