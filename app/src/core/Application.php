<?php

namespace Core;

use UnexpectedValueException;

class Application
{
    /**
     * Initial project application
     *
     * @return bool
     */
    public function run(): bool
    {
        $appFactory = $this->createAppFactory();
        $appFactory->createIniSet()->displayErrors();
        $appFactory->createConfig()->getConfigs();

        return true;
    }

    /**
     * @return ApplicationFactoryInterface
     */
    private function createAppFactory(): ApplicationFactoryInterface
    {
        // возможно безопасней, если на проде будет:
        // return new ApplicationFactoryProdEnv()

        return match (getenv('APP_ENV')) {
            'DEV' => new ApplicationFactoryDevEnv(),
            'PROD' => new ApplicationFactoryProdEnv(),
            'TEST' => new ApplicationFactoryTestEnv(),
            default => throw new UnexpectedValueException('Invalid argument'),
        };
    }
}
