<?php

namespace Core;

use Core\Configs\Config;
use Core\PhpIni\IniSetDevEnv;
use Core\Configs\ConfigDevEnv;
use Core\PhpIni\IniSetInterface;

class ApplicationFactoryDevEnv implements ApplicationFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createConfig(): Config
    {
        return new ConfigDevEnv();
    }

    /**
     * @inheritDoc
     */
    public function createIniSet(): IniSetInterface
    {
        return new IniSetDevEnv();
    }
}
