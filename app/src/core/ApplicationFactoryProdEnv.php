<?php

namespace Core;

use Core\Configs\Config;
use Core\PhpIni\IniSetProdEnv;
use Core\Configs\ConfigProdEnv;
use Core\PhpIni\IniSetInterface;

class ApplicationFactoryProdEnv implements ApplicationFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createConfig(): Config
    {
        return new ConfigProdEnv();
    }

    /**
     * @inheritDoc
     */
    public function createIniSet(): IniSetInterface
    {
        return new IniSetProdEnv();
    }
}
