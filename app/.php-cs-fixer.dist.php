<?php
$finder = PhpCsFixer\Finder::create()->in(__DIR__ . '/src');

$config = new PhpCsFixer\Config();

return $config->setRules([
    '@PSR12' => true,
    'array_syntax' => ['syntax' => 'short'],
])->setFinder($finder)
    ->setCacheFile(__DIR__ . '/cache/.php_cs-fixer.cache')
    ->setUsingCache(true);
