<?php

namespace Tests\unit\core;

use Core\PhpIni\IniSetDevEnv;
use Core\PhpIni\IniSetTestEnv;
use Core\PhpIni\IniSetProdEnv;
use Core\Configs\ConfigDevEnv;
use Core\Configs\ConfigProdEnv;
use Core\Configs\ConfigTestEnv;
use Core\ApplicationFactoryDevEnv;
use Core\ApplicationFactoryProdEnv;
use Core\ApplicationFactoryTestEnv;

class ApplicationFactoryTest extends \Codeception\Test\Unit
{

    /**
     * @dataProvider applicationFactoryProvider
     * @param string $factory
     * @param array<string, mixed> $expected
     */
    public function testApplicationFactory(string $factory, array $expected): void
    {
        $appEnvFactory = new $factory();
        $this->assertInstanceOf(expected: $expected['configClass'], actual: $appEnvFactory->createConfig());
        $this->assertInstanceOf(expected: $expected['iniSetClass'], actual: $appEnvFactory->createIniSet());
    }

    /**
     * @return array[]
     */
    public function applicationFactoryProvider(): array
    {
        return [
            'Dev' => [
                'factory' => ApplicationFactoryDevEnv::class,
                'expected' =>
                    [
                        'configClass' => ConfigDevEnv::class,
                        'iniSetClass' => IniSetDevEnv::class,
                    ],
            ],
            'Prod' => [
                'factory' => ApplicationFactoryProdEnv::class,
                'expected' =>
                    [
                        'configClass' => ConfigProdEnv::class,
                        'iniSetClass' => IniSetProdEnv::class,
                    ],
            ],
            'Test' => [
                'factory' => ApplicationFactoryTestEnv::class,
                'expected' =>
                    [
                        'configClass' => ConfigTestEnv::class,
                        'iniSetClass' => IniSetTestEnv::class,
                    ],
            ],
        ];
    }
}