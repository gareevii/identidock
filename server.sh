#!/bin/bash

if [ "$APP_ENV" = 'PROD' ]; then
  echo "Production Server doesn't installed" # Запуск сервера для эксплуатации (прод)
fi
if [ "$APP_ENV" = 'DEV' ]; then
  echo "Running Development Server" # Запуск сервера для разработки
  exec php -S "$APP_HOST":"$APP_PORT" -t src/
else
  echo "Undefined environment variable"
fi