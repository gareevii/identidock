# TODO: настроить автоматическое создание этой бд
# пока создать руками, фокус с ./mysql/init в services.mysql.volumes не прошел, нужно разбираться

# create databases
CREATE DATABASE IF NOT EXISTS `generatedata`;

# create root user and grant rights
CREATE USER 'root'@'localhost' IDENTIFIED BY 'local';
GRANT ALL ON *.* TO 'root'@'%';